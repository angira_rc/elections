import os
from pymongo import MongoClient
from random import choice

def conn(collection):
    client = MongoClient();
    db = client.haul
    return db[collection]

def status(status):
    if status:
        return "Success"

    return "A problem was encountered"

def crypt(str, hash):
    if hash == "sha1":
        from hashlib import sha1
        str = sha1(str.encode('utf-8'))
    elif hash == "sha224":
        from hashlib import sha224
        str = sha224(str.encode('utf-8'))
    elif hash == "sha256":
        from hashlib import sha256
        str = sha256(str.encode('utf-8'))
    elif hash == "sha384":
        from hashlib import sha384
        str = sha384(str.encode('utf-8'))
    elif hash == "sha512":
        from hashlib import sha512
        str = sha512(str.encode('utf-8'))
    elif hash == "md5":
        from hashlib import md5
        str = md5(str.encode('utf-8'))

    str = str.hexdigest()
    return str

def hasher(str):
    order = []
    hashes = ["sha224", "md5", "sha384", "sha1", "sha512", "sha256"]
    for x in range(4):
        hash = choice(hashes)
        order.append(hashes.index(hash))
        str = crypt(str, hash)

    return [str, order]

def verify(uname, str):
    col = conn("users")
    user = col.find_one({"uname": uname})
    if user:
        hashes = ["sha224", "md5", "sha384", "sha1", "sha512", "sha256"]
        for pos in user["sequence"]:
            str = crypt(str, hashes[pos])

        if str == user["password"]:
            return True

    return False

def init():
    client = MongoClient();
    db = client.haul
    print("Initializing database...\n")
    users_col = db.users
    adm = users_col.find_one({"type": "admin"})
    if not adm:
        password = 'pwd'
        conf = 'conf'
        while not password == conf:
            password = input("Specify the admin password : ")
            conf = input("Confirm password : ")
            if password == conf:
                pwd = hasher(password)
                admin = {"uname": "root", "name": "root", "password": pwd[0], "sequence": pwd[1], "type": "admin", "station": "base"}
                print("Creating root user...\n")
                status = users_col.insert_one(admin)
                if status:
                    db.create_collection("students")
                    print("Success\n")
                    print("Creating Tally records...\n")
                    db.create_collection("tally")
                    print("Success\n")
                    print("Creating Level records...\n")
                    db.create_collection("levels")
                    print("Success\n")
                    print("Creating Voter records...\n")
                    db.create_collection("voters")
                    print("Success\n")
                    return True

                print("A problem was encountered")
                return False

            print("Your passwords don't match\n")
            return False

    print("System already initialized")
    return False

def rand_choice(space, chosen):
    while True:
        new_choice = choice(space)
        if new_choice not in chosen:
            return new_choice

def view_students(st_id = None):
    col = conn("students")
    result = []
    students = []

    if st_id:
        students = col.find({"st_id": {"$regex": st_id, "$options": "i"}}).limit(5)
    else:
        students = col.find()

    for student in students:
        std = {'st_id': student['st_id'], 'name': student['name']}
        result.append(std)

    return result

def dir(sc_id):
    path = f'./store/voters/{sc_id}/'
    if not os.path.exists(path):
        os.makedirs(path)

    return True

def face_rec(sc_id):
    import face_recognition as face
    from glob import glob
    images = glob(f'./store/voters/{sc_id}/*.jpg')
    old_enc = []
    for img in images:
        load = face.load_image_file(img)
        old_enc.append(face.face_encodings(load)[0])

    load = face.load_image_file(f'./store/voters/{sc_id}/temp/temp.jpg')
    new_enc = face.face_encodings(load)[0]

    result = face.compare_faces(old_enc, new_enc, 0.4)

    if result[0] == True:
        return True
    else:
        return False
