import modules as mod

# Registration
def register(sc_id, name, level, station, images):
    col = mod.conn('voters')
    exists = col.find_one({ 'sc_id': sc_id })
    if not exists:
        col = mod.conn('users')
        stat_exists = col.find_one({ 'station': station })
        if stat_exists:
            status = mod.dir(sc_id)
            if (status):
                # Code to move images to ./store/voters/{sc_id}/temp
                voter = {
                            'sc_id': sc_id,
                            'name': name,
                            'station': station,
                            'levels': {
                                level: {
                                    'id': mod.hasher(sc_id)[0],
                                    'voted': False
                                }
                            }
                        }

                col = mod.conn('voters')
                status = col.insert_one(voter)
                if status:
                    return voter['levels'][level]['id']

            return f"There was a problem registering the voter '{name}'"

        return f"The station '{station}' doesn't exist"

    return f"The voter '{name}' already exists"

def registered(station='all'):
    col = mod.conn('voters')
    voters = col.find()

    final = []
    for voter in voters:
        vtr = {
        'sc_id': voter['sc_id'],
        'name': voter['name'],
        'levels': voter['levels'],
        'station': voter['station']
        }

        if station == 'all':
            final.append(vtr)
        else :
            if voter['station'] == station:
                final.append(vtr)

    return final

# Voting
def face_validate(sc_id, image):
    col = mod.conn('voters')
    exists = col.find_one({ 'sc_id': sc_id })
    # Code to move image to ./store/voters/{sc_id}/temp
    if exists and mod.face_rec(sc_id):
        return True

    return False

def ballot(sc_id, level):
    col = mod.conn('voters')
    voter = col.find_one({ 'sc_id': sc_id })
    col = mod.conn('levels')
    lvl = col.find_one({ 'name': level })

    if voter and lvl:
        from random import shuffle, choice
        ballot = {}

        if level in voter['levels']:
            candidates = []
            symbols = ['!','@','#','$','%','^','&','*','~','<','>']
            salt = list("candidates")
            chosen = []

            for category in lvl['categories']:
                items = []
                x = 1
                while x <= len(list(lvl['categories'][category].keys())):
                    items.append(x)
                    x += 1

                if len(items) == 0:
                    items.append(0)

                shuffle(items)
                candidates.append(items)
                chosen.append(mod.rand_choice(symbols, chosen))

            voter_hash = list(voter['levels'][level]['id'])

            for symbol in symbols:
                if symbol in chosen:
                    symbols.remove(symbol)

            voter_hash += symbols
            voter_hash += chosen
            shuffle(voter_hash)

            for pos, val in enumerate(chosen):
                hash_to_str = ''
                for psn in candidates[pos]:
                    hash_to_str += str(psn)

                cat_hash = choice(salt) + str(pos) + choice(symbols) + hash_to_str + choice(symbols) + choice(symbols)
                val += cat_hash
                chosen[pos] = val

            voter_hash += chosen
            shuffle(voter_hash)
            hash_to_str = ''

            for item in voter_hash:
                hash_to_str += item

            return hash_to_str

        return f"The student `{voter['name']}` is not registered for the `{level}` elections"

    return 'Invalid details specified'

def vote(sc_id, level, ballot):
    col = mod.conn('voters')
    voter = col.find_one({ 'sc_id': sc_id })
    col = mod.conn('levels')
    lvl = col.find_one({ 'name': level })

    if voter and lvl:
        from re import findall
        votes = findall(r'\W[a-z][0-9]\W[0-9]+\W\W', ballot)

        cats = lvl['categories']
        selected = {}
        for vote in votes:
            final_choices = int(vote[4:-2])
            cat = list(cats.keys())[int(vote[2:3])]
            if final_choices != 0:
                final_choices = list(str(final_choices))
                for pos, item in enumerate(final_choices):
                    final_choices[pos] = int(item)

                choices = list(cats[cat].keys())
                init_choices = []

                x = 1
                while x <= len(list(cats[cat].keys())):
                    init_choices.append(x)
                    x += 1

                for choice in init_choices:
                    if choice not in final_choices:
                        chosen = list(cats[cat].keys())[choice]
                        status = col.update_one({'sc_id': sc_id}, {'$inc': {f'categories.{cat}.{chosen}.votes': 1}})
                        if status:
                            selected[cat] = chosen
                        else:
                            return 'A problem was encountered'

            else:
                selected[cat] = 'none'

        col = mod.conn('voters')
        status = col.update_one({'sc_id': sc_id}, {'$set': {f'levels.{level}.voted': True}})
        if status:
            col = mod.conn("tally")
            vtr_id = ''
            for item in ballot:
                vtr_id += str(item)

            status = col.insert_one({"voter_id": vtr_id, "levels": {level: selected}})
            if status:
                return vtr_id

    return False

def results(level):
    col = mod.conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        return this_level

    return f"The level '{level}' wasn't found"
