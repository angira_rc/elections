from modules import status as st, conn, verify

# Users
def view_users():
    col = conn("users")
    users = col.find({"type": {"$ne":"admin"}})
    result = []
    for user in users:
        us = {'uname': user['uname'], 'name': user['name'], 'type': user['type'], 'station': user['station']}
        result.append(us)

    return result

def create_user(uname, name, password, station):
    col = conn("users")
    usr = col.find_one({"uname": uname})

    if not usr:
        from modules import hasher
        pwd = hasher(password)
        user = {"uname": uname, "name": name, "password": pwd[0], "sequence": pwd[1], "type": "active", "station": station}
        status = col.insert_one(user)
        return st(status)

    return f"The user '{uname}' already exists'"

def edit_user(user, edited, prev):
    col = conn("users")
    usr = col.find_one({'uname': prev})

    if usr:
        for attr in edited:
            usr[attr] = user[attr]
            if attr == "password":
                from modules import hasher
                pwd = hasher(usr[attr])
                usr[attr] = pwd[0]
                usr["sequence"] = pwd[1]

        status = col.replace_one({'_id': usr['_id']}, usr)
        return st(status)

    return f"The user '{prev}' was not found"

def switch(uname):
    col = conn("users")
    user = col.find_one({'uname': uname})

    if user:
        if user['type'] == 'active':
            status = col.update_one({"uname": uname}, {"$set": {"type": "inactive"}})
        elif user['type'] == 'inactive':
            status = col.update_one({"uname": uname}, {"$set": {"type": "active"}})
        return st(status)

    return f"The user '{uname}' was not found"

def delete_user(uname):
    col = conn("users")
    user = col.find_one({'uname': uname})

    if user:
        status = col.delete_one({"uname": uname})
        return st(status)

    return f"The user '{uname}' was not found"

# levels
def view_levels(level=None):
    col = conn("levels")

    if level:
        lv = col.find_one({"name": level})
        if lv:
            return lv

        return f"The level '{level}' doesn't exist"
    else:
        levels = col.find()
        return levels

def new_level(name):
    col = conn("levels")
    this_level = col.find_one({"name": name})
    if not this_level:
        lvl = {"name": name, "status": "pending", "categories": {}}
        status = col.insert_one(lvl)
        return st(status)

    return f"The level '{name}' already exists"

def edit_level(name, new_name):
    col = conn("levels")
    this_level = col.find_one({"name": name})
    if this_level:
        status = col.update_one({"name": name}, {"$set": {"name": new_name}})
        return st(status)

    return f"The level '{name}' doesn't exist"

def delete_level(name):
    col = conn("levels")
    this_level = col.find_one({"name": name})
    if this_level:
        status = col.delete_one({"name": name})
        return st(status)

    return f"The level '{name}' wasn't found"

def new_category(name, level):
    col = conn("levels")
    this_level = col.find_one({"name": level})
    if this_level:
        if name not in this_level["categories"]:
            this_level["categories"][name] = {}
            status = col.update_one({"name": level}, {"$set": {"categories": this_level["categories"]}})
            return st(status)

        return f"The category '{name}' already exists"

    return f"The level '{level}' wasn't found"

def edit_category(name, level, new_name):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if name in this_level["categories"]:
            cats = this_level["categories"]
            cats[new_name] = cats.pop(name)
            status = col.update_one({"name": level}, {"$set": {"categories": cats}})
            return st(status)

        return f"The category '{name}' doesn't exist"

    return f"The level '{level}' doesn't exist"

def delete_category(name, level):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if name in this_level["categories"]:
            cats = this_level["categories"]
            del cats[name]
            status = col.update_one({"name": level}, {"$set": {"categories": cats}})
            return st(status)

        return f"The category '{name}' wasn't found"

    return f"The level '{level}' wasn't found"

def view_candidates(level, category=None):
    col = conn("levels")

    this_level = col.find_one({"name": level})

    if this_level:
        if category:
            if category in this_level["categories"]:
                return this_level["categories"][category]

            return f"The category '{category}' wasn't found"

        return this_level["categories"]

    return f"The level '{level}' wasn't found"

def new_candidate(cand, level, category):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if category in this_level["categories"]:
            exists = False
            for candidate in this_level["categories"][category]:
                if candidate == cand:
                    exists = True

            if not exists:
                this_level["categories"][category][cand] = {"votes": 0}
                status = col.update_one({'name': level}, {"$set": {"categories": this_level["categories"]}})
                return st(status)

            return f"The candidate '{cand}' already exists"

        return f"The category '{category}' wasn't found"

    return f"The level '{level}' wasn't found"

def edit_candidate(cand, prev, level, category):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if category in this_level["categories"]:
            for candidate in this_level["categories"][category]:
                if candidate == prev:
                    this_level["categories"][category][cand] = this_level["categories"][category].pop(prev)
                    status = col.update_one({'_id': this_level['_id']}, {"$set": {"categories": this_level["categories"]}})
                    return st(status)

            return f"The candidate '{cand}' wasn't found"

        return f"The category '{category}' wasn't found"

    return f"The level '{level}' wasn't found"

def delete_candidate(name, level, category):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if category in this_level["categories"]:
            for candidate in this_level["categories"][category]:
                if candidate == name:
                    del this_level["categories"][category][name]
                    status = col.update_one({'_id': this_level['_id']}, {"$set": {"categories": this_level["categories"]}})
                    return st(status)

            return f"The candidate '{cand}' wasn't found"

        return f"The category '{category}' wasn't found"

    return f"The level '{level}' wasn't found"

# Voters
def voter_reg(level, details):
    col = conn('voters')
    voter = col.find_one({'sch_id': details['sch_id']})

    if not voter:
        reg = {'sch_id': details['sch_id'], 'name': details['name'], 'levels': {level: {'voted': False, 'vote': {}}}, 'eligible': True}
        status = col.insert_one(reg)
        return st(status)

    return "The voter `details['name']` already registered"

def view_registered(level):
    col = conn("voters")
    voters = col.find()
    result = []

    for voter in voters:
        if level in voter['levels']:
            # voter_levels = voter['levels']
            vtr = { 'sch_id': voter['sch_id'], 'name': voter['name'], 'levels': voter['levels'] }
            result.append(vtr)

    return result

# def ballot(level):

# def cast_vote(voter, vote):

# Results
# def results(level, category=None, candidate=None):
