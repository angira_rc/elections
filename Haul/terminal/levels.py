from modules import status as st, conn

def view_levels(level=None):
    col = conn("levels")

    if level:
        lv = col.find_one({"name": level})
        if lv:
            x = 1
            blank = "   Empty"
            print(f"{level}")
            for category in lv["categories"]:
                blank = ""
                print(f"    {x}. {category}")
                x = x + 1

            print(blank)
        else:
            print(f"The level `{level}` doesn't exist")
    else:
        levels = col.find()
        x = 1
        for lv in levels:
            y = 1
            blank = "   Empty"
            print(f"{x}. {lv['name']}")
            for category in lv["categories"]:
                blank = ""
                print(f"    {y}. {category}")
                y = y + 1

            print(blank)
            x = x + 1

def new_level(name):
    col = conn("levels")
    this_level = col.find_one({"name": name})
    if not this_level:
        print("Creating new Level...\n")
        lvl = {"name": name, "status": "pending", "categories": {}}
        status = col.insert_one(lvl)
        st(status)
    else:
        print(f"The level {name} already exists")

def edit_level(name, new_name):
    col = conn("levels")
    this_level = col.find_one({"name": name})
    if this_level:
        status = col.update_one({"name": name}, {"$set": {"name": new_name}})
        st(status)
    else:
        print(f"The level {name} doesn't exist\n")
        add = "N"
        add = input("Add this level? (Y/N) : ")
        if add == "Y":
            new_level(new_name)

def delete_level(name):
    col = conn("levels")
    print(f"Finding the Level {name}...\n")
    this_level = col.find_one({"name": name})
    if this_level:
        status = col.delete_one({"name": name})
        st(status)
    else:
        print(f"The level {level} wasn't found")

def new_category(name, level):
    col = conn("levels")
    print(f"Finding the Level {level}...\n")
    this_level = col.find_one({"name": level})

    if not this_level:
        new_level(level)
        this_level = col.find_one({"name": level})

    print("Creating new category...\n")
    if name not in this_level["categories"]:
        cats = this_level["categories"]
        new_cat = {}
        cats[name] = new_cat
        status = col.update_one({"name": level}, {"$set": {"categories": cats}})
        st(status)
    else:
        print(f"The category {name} already exists")

def edit_category(name, level, new_name):
    col = conn("levels")
    this_level = col.find_one({"name": level})

    if this_level:
        if name in this_level["categories"]:
            cats = this_level["categories"]
            cats[new_name] = cats.pop(name)
            status = col.update_one({"name": level}, {"$set": {"categories": cats}})
            st(status)
        else:
            print(f"The category {name} doesn't exist\n")
            add = "N"
            add = input("Add this category? (Y/N) : ")
            if add == "Y":
                new_category(new_name, level)
    else:
        print(f"The level {level} doesn't exist\n")
        add = "N"
        add = input("Add this level? (Y/N) : ")
        if add == "Y":
            new_level(level)

def delete_category(name, level):
    col = conn("levels")
    print(f"Finding the Level {level}...\n")
    this_level = col.find_one({"name": level})

    if this_level:
        print("Deleting category...\n")
        if name in this_level["categories"]:
            cats = this_level["categories"]
            del cats[name]
            status = col.update_one({"name": level}, {"$set": {"categories": cats}})
            st(status)
        else:
            print(f"The category {name} wasn't found")
    else:
        print(f"The level {level} wasn't found")

def view_candidates(level=None, category=None):
    col = conn("levels")

    if level:
        this_level = col.find_one({"name": level})
        print(level)
        blank_lev = "   Blank"
        if category:
            blank_lev = ""
            print(f"    {category}")
            x = 1
            for candidate in this_level["categories"][category]:
                print(f"        {x}. {candidate}")
                x = x + 1
        else:
            x = 1
            blank_lev = "   Blank"
            for category in this_level["categories"]:
                blank_lev = ""
                print(f"    {x}. {category}")
                y = 1
                blank_cat = "       Blank"
                for candidate in this_level["categories"][category]:
                    blank_cat = ""
                    print(f"        {y}. {candidate}")
                    y = y + 1
                print(blank_cat)
                x = x + 1
            print(blank_lev)
    else:
        levels = col.find()
        x = 1
        for level in levels:
            y = 1
            blank = "   Empty"
            print(f"{x}. {level['name']}")
            for category in level["categories"]:
                blank = ""
                print(f"    {y}. {category}")
                y = y + 1

            print(blank)
            x = x + 1
