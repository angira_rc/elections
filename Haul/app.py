from flask import Flask, request, session, jsonify
from flask_cors import CORS, cross_origin
from pymongo import MongoClient
import admin as adm
import station as st
import modules as mod

app = Flask(__name__)
cors = CORS(app)
session

app.config['CORS_HEADERS'] = 'Content-Type'

def auth(auth_usrs):
    if 'type' in session:
        if session['type'] in auth_usrs:
            return True

    return False

# Login
@app.route('/login', methods=['POST'])
def login():
    cred = request.get_json()
    response = {'status': False,
                'msg': 'Malformed request'}

    if 'uname' in cred and 'pwd' in cred:
        if mod.verify(cred['uname'], cred['pwd']):
            client = MongoClient()
            col = client.haul.users
            usr = col.find_one({'uname': cred['uname']})

            if usr['type'] == 'inactive':
                response['msg'] = 'Your account is inactive. Contact admin for futher detals.'
            else:
                session['type'] = usr['type']
                session['uname'] = cred['uname']

                response = {'status': True,
                            'data': {'uname': usr['uname'],
                                     'name': usr['name'],
                                     'type': usr['type'],
                                     'station': usr['station']},
                            'msg':'Ok'}

        else:
            response['msg'] = 'Wrong UserName or Password'

    return jsonify({'response': response})

@app.route('/logout')
def logout():
    session.pop('type', None)
    session.pop('uname', None)
    return jsonify({'response': {'status': True, 'msg': 'Logged Out'}})

# Users
@app.route('/users', methods=['GET', 'POST'])
def users():
    if auth(['admin']):
        if request.method ==  'GET':
            response = adm.view_users()
            return jsonify({'response': {'status': True, 'data': response}})

        else:
            data = request.get_json()
            print(data)
            response = 'Malformed request'
            status = False

            if data['action'] == 'create':
                response = adm.create_user(data['uname'], data['name'], data['password'], data['station'])
                status = True

            elif data['action'] == 'switch':
                response = adm.switch(data['uname'])
                status = True

            elif data['action'] == 'edit':
                response = adm.edit_user(data['new'], data['edited'], data['prev'])
                status = True

            elif data['action'] == 'delete':
                response = adm.delete_user(data['uname'])
                status = True

            return jsonify({'response': {'status': status, 'data': response}})


    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})

# Levels
@app.route('/levels', methods=['GET', 'POST'])
def levels():
    if auth(['active', 'admin']):
        if request.method == 'GET':
            response = []
            for level in adm.view_levels():
                lv = {'name': level['name'], 'categories': level['categories'], 'status': level['status']}
                response.append(lv)

            return jsonify({'response': {'status': True, 'data': response}})

        else:
            data = request.get_json()
            response = 'Malformed request'
            status = False

            if data['action'] == 'create':
                response = adm.new_level(data['name'])
                status = True

            elif data['action'] == 'edit':
                response = adm.edit_level(data['name'], data['new_name'])
                status = True

            elif data['action'] == 'delete':
                response = adm.delete_level(data['name'])
                status = True

            return jsonify({'response':{'status': status, 'data': response}})

    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})

@app.route('/levels/<string:name>', methods=['GET', 'POST'])
def level(name):
    if auth(['admin']):
        if request.method == 'GET':
            level = adm.view_levels(name)
            if type(level) == type(dict()):
                response = {'name': level['name'], 'categories': level['categories'], 'status': level['status']}
                return jsonify({'response': response})
            else:
                return jsonify({'response': level})

        else:
            data = request.get_json()
            response = 'Malformed request'
            status = False

            if data['action'] == 'create':
                response = adm.new_category(data['category'], name)
                status = True

            elif data['action'] == 'edit':
                response = adm.edit_category(data['category'], name, data['new_name'])
                status = True

            elif data['action'] == 'delete':
                response = adm.delete_category(data['category'], name)
                status = True

            return jsonify({'response': {'status': status, 'data': response}})

    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})

@app.route('/levels/<string:level>/categories', methods=['GET', 'POST'])
def categories(level):
    if auth(['admin']):
        if request.method == 'GET':
            cats = adm.view_candidates(level)
            return jsonify({'response': {'status': True, 'data': cats}})

        else:
            data = request.get_json()
            reponse = 'Malformed request'
            status = False

            if data['action'] == 'create':
                response = adm.new_candidate(data['candidate'], level, data['category'])
                status = True

            elif data['action'] == 'edit':
                response = adm.edit_candidate(data['new'], data['prev'], level, data['category'])
                status = True

            elif data['action'] == 'delete':
                response = adm.delete_candidate(data['name'], level, data['category'])
                status = True

            return jsonify({'response': {'status': status, 'data': response}})

    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})

# Students
@app.route('/students', methods=['POST'])
def students():
    if auth(['active', 'admin']):
        data = request.get_json()
        reponse = 'Malformed request'
        status = False

        students = mod.view_students(data['id'])
        return jsonify({'response': {'status': True, 'data': students}})
        # return jsonify({'response': {'status': status, 'data': response}})

    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})

# Voter
@app.route('/voters/<string:station>', methods=['GET', 'POST'])
def voters_st(station):
    if auth(['active', 'admin']):
        if request.method == 'GET':
            reg_voters = st.registered(station)
            return jsonify({'response': {'status': True, 'data': reg_voters}})

        else:
            data = request.get_json()
            reponse = 'Malformed request'
            status = False

            if data['action'] == 'register':

                response = st.register(data['sc_id'], data['name'], data['level'], station)
                status = True

            return jsonify({'response': {'status': status, 'data': response}})

    return jsonify({'response': {'status': False, 'msg': 'You are not authorized to use this sevice'}})




if __name__ == '__main__':
    app.secret_key = 'random_key'
    app.run(debug=True)
