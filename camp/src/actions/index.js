export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const NEW_VIEW = 'NEW_VIEW'
export const SET_URL = 'SET_URL'
export const SUB_VIEW = 'SUB_VIEW'
export const MINI_VIEW = 'MINI_VIEW'
export const GET_LEVELS = 'GET_LEVELS'
export const GET_USERS = 'GET_USERS'
export const GET_VOTERS = 'GET_VOTERS'
export const SET_NEW = 'SET_NEW'
export const SAVE = 'SAVE'

export function login (usr) {
  const action = {
    type: LOGIN,
    usr: usr
  }
  return action
}

export function logout (usr) {
  const action = {
    type: LOGOUT,
    usr: usr
  }
  return action
}

export function new_view (arg) {
  const action = {
    type: NEW_VIEW,
    usr: arg.usr,
    view: arg.view
  }
  return action
}

export function subview (parent, sub) {
  const action = {
    type: SUB_VIEW,
    parent: parent,
    subview: sub
  }
  return action
}

export function miniview(m_view) {
  const action = {
    type: MINI_VIEW,
    miniview: m_view
  }
  return action
}

export function setUrl (url) {
  const action = {
    type: SET_URL,
    url: url
  }
  return action
}

export function getLevels(data) {
  const action = {
    type: GET_LEVELS,
    data: data
  }
  return action
}

export function getUsers(data) {
  const action = {
    type: GET_USERS,
    data: data
  }
  return action
}

export function getVoters(data) {
  const action = {
    type: GET_VOTERS,
    data: data
  }
  return action
}

export function unsaved(item, data) {
  const action = {
    type: SET_NEW,
    item: item,
    data: data
  }
  return action
}

export function save (item) {
  const action = {
    type: SAVE,
    item: item
  }
  return action
}
