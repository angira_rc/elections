import { LOGIN } from '../actions'

function accounts (state = { uname: null, name: null, type: null, station: null }, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        uname: action.usr.uname,
        name: action.usr.name,
        type: action.usr.type,
        station: action.usr.station
      }

    default:
      return state
  }
}

export default accounts
