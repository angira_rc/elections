import { combineReducers } from 'redux'
import accounts from './accounts.js'
import views from './views.js'
import settings from './settings.js'
import data from './data.js'

const main = combineReducers({
  accounts,
  views,
  settings,
  data
})

export default main
