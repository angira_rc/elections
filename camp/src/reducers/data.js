import { GET_LEVELS, GET_USERS, GET_VOTERS, SET_NEW } from '../actions'

function data (state = { unsaved: { status: false, data: {} } }, action) {
  switch (action.type) {
    case GET_LEVELS:
      return {
        ...state,
        levels: action.data
      }

    case GET_USERS:
      return {
        ...state,
        users: action.data
      }

    case GET_VOTERS:
      return {
        ...state,
        voters: action.data
      }

    case SET_NEW:
      let data = { ...state.unsaved.data }
      data[action.item] = action.data
      
      return {
        ...state,
        unsaved: { status: true, ...data }
      }

    default:
      return state
  }
}

export default data
