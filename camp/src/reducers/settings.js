import { SET_URL } from '../actions'

function settings (state = { url: 'http://localhost:5000'}, action) {
  switch (action.type) {
    case SET_URL:
      let new_state = { ...state, url: action.url }
      return new_state

    default:
      return state
  }
}

export default settings
