import { NEW_VIEW, SUB_VIEW, MINI_VIEW } from '../actions'

function views (state = { name: 'login', menu: [], current: 'login', miniview: null }, action) {
  switch (action.type) {
    case NEW_VIEW:
      switch (action.usr) {
        case 'admin':
          return {
            ...state,
            name: 'admin',
            menu: [
              { id: 'levels', name: 'Levels' },
              { id: 'users', name: 'Users' },
              { id: 'elections', name: 'Elections' }
            ],
            current: 'levels'
          }

        case 'active':
          return {
            ...state,
            name: 'station',
            menu: [
              { id: 'voter-reg', name: 'Voter Registration'},
              { id: 'elections', name: 'Elections' }
            ],
            current: ''
          }

        default:
          if (action.view === 'settings') {
            return {
              ...state,
              name: 'settings',
              menu: [
                { id: 'network', name: 'Network'}
              ],
              current: 'network'
            }
          }

          return state
      }

    case SUB_VIEW:
      return {
        ...state,
        current: action.subview
      }

    case MINI_VIEW:
      return {
        ...state,
        miniview: action.miniview
      }

    default:
      return state
  }
}

export default views
