export async function request (url, method) {
  let resp
  let request = {
    method: method,
    mode: "cors",
    credentials: "same-origin",
    headers: {
        "Content-Type": "application/json; charset=utf-8",
    }
  }

  if (method === 'POST') {
    request = { ...request, body: JSON.stringify(data) }
  }

  await fetch(
    url,request
  ).then(response => response.json())
  .then(function(json){ resp = json })
  .catch(error => resp = { status: false, msg: "There was an error communicating with the server." });

  return resp
}

function exists (arr, str) {
  for (var pos in arr) {
    if (arr[pos] === str) {
      return true
    }
  }
  return false
}

export function show_hide(id, starter) {
  let element = document.getElementById(id)
  let classes = element.className.split(" ")

  if (exists(classes, "expanded")) {
    element.classList.remove("expanded")
    setTimeout(() => element.style.display = 'none', 400)
  } else {
    element.classList.add("expanded")
    element.style.display = 'block'
  }

  if (starter.innerHTML === '≚') {
    starter.innerHTML = '≙'
  } else if (starter.innerHTML === '≙') {
    starter.innerHTML = '≚'
  }
}
