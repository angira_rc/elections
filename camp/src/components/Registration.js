import React, { Component } from 'react'
import { connect } from 'react-redux'
import { request } from '../modules'
import AccBar from './AccBar'
import Search from './Search'
import RegForm from './RegForm'
import Menu from './Menu'

class Registration extends Component {
  async registered () {
    // this.setState({ago: { status: true, msg: "" }})
    // let data = { uname: this.state.uname, pwd: this.state.pwd }
    let req = await request(`${this.props.settings.url}/voters/${this.props.accounts.station}`, 'GET')
    let response = req.response
    console.log(response)
  }

  async componentWillMount() {
    await this.registered()
  }

  render () {
    switch (this.props.views.miniview) {
      case "register":
        return (
          <div className="Registration view">
            <Menu />
            <div className="content">
              <h2>Registration</h2>
              <br/>
              <RegForm />
            </div>
          </div>
        )

      case "registered":
        return (
          <div className="Registration view">
            <Menu />
            <div className="content">
              <h2>Registered Voters</h2>
              <br/>
              <RegForm />
            </div>
          </div>
        )

      default:
        return (
          <div className="Registration view">
            <Menu />
            <div className="content">
              <h2>Registration</h2>
              <br/>
              <Search />
            </div>
          </div>
        )
    }
  }
}

function mapStatetoProps (state) {
  return {
    data: state.data,
    accounts: state.accounts,
    settings: state.settings,
    views: state.views
  }
}

export default connect(mapStatetoProps, null)(Registration)
