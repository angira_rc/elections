import React, { Component } from 'react'
import { connect } from 'react-redux'

class AccBar extends Component {
  render () {
    return (
      <div className="AccBar">
        <div id="account">
          <a>{ this.props.user.name }</a>
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    user: state.accounts
  }
}

export default connect(mapStatetoProps, null)(AccBar)
