import React, { Component } from 'react'
import { connect } from 'react-redux'
import Admin from './Admin'
import Station from './Station'
import Settings from './Settings'
import Login from './Login'
import './View.css'

class View extends Component {
  render () {
    switch (this.props.view.name) {
      case 'admin':
        return (
          <Admin />
        )

      case 'station':
        return (
          <Station />
        )

      case 'settings':
        return (
          <Settings />
        )

      default:
        return (
          <Login />
        )
    }
  }
}

function mapStatetoProps (state) {
  return {
    view: state.views
  }
}

export default connect(mapStatetoProps, null)(View)
