import React, { Component } from 'react'
import { Form, FormControl, ControlLabel, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { save } from '../actions'
import { request } from '../modules'

class RegForm extends Component {
  render () {
    return (
      <div className="RegForm miniview">
        <Form id="RegForm">
          <table>
            <tbody>
              <tr>
                <td><ControlLabel>Student Id</ControlLabel></td>
                <td><FormControl disabled = { true } value= { this.props.data.unsaved.voter.st_id } /></td>
              </tr>
              <tr>
                <td><ControlLabel>Name</ControlLabel></td>
                <td><FormControl disabled = { true } value= { this.props.data.unsaved.voter.name } /></td>
              </tr>
              <tr>
                <td><ControlLabel>Level</ControlLabel></td>
                <td>
                  <FormControl
                    componentClass="select">
                    {
                      this.props.data.levels.map((level) => {
                        return (
                          <option key={ level.name } value = { level.name}>{ level.name }</option>
                        )
                      })
                    }
                  </FormControl>
                </td>
              </tr>
              <tr>
                <td>
                  <Button>Save</Button>
                </td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </Form>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    data: state.data
  }
}

export default connect(mapStatetoProps, { save })(RegForm)
