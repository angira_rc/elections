import React, { Component } from 'react'
import { connect } from 'react-redux'
import Levels from './Levels'
import Users from './Users'
import Elections from './AdmElections'

class Admin extends Component {
  render () {
    switch (this.props.view.current) {
      case 'levels':
        return (
          <Levels />
        )

      case 'users':
        return (
          <Users />
        )

      case 'elections':
        return (
          <Elections />
        )

      default:
        return (
          <Levels />
        )
    }
  }
}

function mapStatetoProps (state) {
  return {
    view: state.views
  }
}

export default connect(mapStatetoProps, null)(Admin)
