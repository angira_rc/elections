import React, { Component } from 'react'
import { connect } from 'react-redux'
import Levels from './Levels'
import Users from './Users'
import StationMenu from './StationMenu'
import Registration from './Registration'
import Elections from './StatElections'

class Station extends Component {
  render () {
    switch (this.props.view.current) {
      case 'voter-reg':
        return (
          <Registration />
        )

      case 'elections':
        return (
          <Elections />
        )

      default:
        return (
          <Registration />
        )
    }
  }
}

function mapStatetoProps (state) {
  return {
    view: state.views
  }
}

export default connect(mapStatetoProps, null)(Station)
