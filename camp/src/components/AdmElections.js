import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { request } from '../modules'
import Menu from './Menu'

class Elections extends Component {
  render () {
    return (
      <div className="AdmElections view">
        <Menu />
        <div className="content">
          <h2>Elections</h2>
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    settings: state.settings
  }
}

export default connect(mapStatetoProps, null)(Elections)
