import React, { Component } from 'react'
import { FormControl, ControlLabel } from 'react-bootstrap'
import { connect } from 'react-redux'
import { show_hide } from '../modules'
import Menu from './Menu'
import './Users.css'

class Users extends Component {
  validate () {

  }

  render () {
    return (
      <div className="Users view">
        <Menu />
        <div className="content">
          <h2>Users</h2>
          <br/>
          <div className="divisions">
            <div className="part">Name</div>
            <div className="part action">UserName</div>
            <div className="part action"></div>
            <div className="part action"></div>
            <div className="part action"></div>
          </div>
          <br/>
          {
            this.props.data.users.map(user => {
              return (
                <div key={ user.uname } className="user">
                  <div className="divisions parent">
                    <div className="part">{ user.name }</div>
                    <div className="part">{ user.uname }</div>
                    <div className="part action">Save</div>
                    <div className="part action delete">&#10008;</div>
                    <div className="part action expand" onClick={(event) => show_hide(user.uname, event.target) }>≚</div>
                  </div>
                  <div className="user-details subview" id={ user.uname }>
                    <div className="details">
                      <ControlLabel>Full Name</ControlLabel>
                      <FormControl
                        defaultValue = { user.name }
                        placeholder = "Full Name"
                        onBlur = {(event) => this.validate(event.target.value, user.name) } />
                      <ControlLabel>UserName</ControlLabel>
                      <FormControl
                        defaultValue = { user.uname }
                        placeholder = "UserName"
                        onBlur = {(event) => this.validate(event.target.value, user.uname) } />
                      <ControlLabel>Station</ControlLabel>
                      <FormControl
                        defaultValue = { user.station }
                        placeholder = "Station"
                        onBlur = {(event) => this.validate(event.target.value, user.station) } />
                      <ControlLabel>Status</ControlLabel>
                      <FormControl
                        componentClass="select"
                        defaultValue = { user.type }
                        onBlur = {(event) => this.validate(event.target.value, user.name) }>
                        <option value="active">active</option>
                        <option value="inactive">inactive</option>
                      </FormControl>
                    </div>
                  </div>
                </div>
              )
            })
          }
          <div className="user">
            <div className="divisions parent">
              <div className="part">Add New</div>
              <div className="part action">Save</div>
              <div className="part action">Clear</div>
              <div className="part action expand" onClick={(event) => show_hide("new", event.target) }>&#10010;</div>
            </div>
            <div className="user-details subview" id="new">
              <div className="details">
                <FormControl
                  placeholder = "Full Name"
                  onBlur = {(event) => this.validate(event.target.value, "new") } />
                <FormControl
                  placeholder = "UserName"
                  onBlur = {(event) => this.validate(event.target.value, "new") } />
                <FormControl
                  type = "password"
                  placeholder = "Password"
                  onBlur = {(event) => this.validate(event.target.value, "new") } />
                <FormControl
                  placeholder = "Station"
                  onBlur = {(event) => this.validate(event.target.value, "new") } />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    data: state.data,
    settings: state.settings
  }
}

export default connect(mapStatetoProps, null)(Users)
