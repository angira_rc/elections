import React, { Component } from 'react'
import { FormControl, ControlLabel } from 'react-bootstrap'
import { connect } from 'react-redux'
import { unsaved, miniview } from '../actions'
import { request } from '../modules'

class Search extends Component {
  constructor () {
    super()
    this.state = {
      students: []
    }
  }

  async search (val) {
    if (val) {
      if (val.length > 5) {
        let req = await request(`${this.props.settings.url}/students`, 'POST', { id: val })
        let response = req.response
        if (response.data) {
          this.setState({students: response.data})
        } else {
          this.setState({students: [{st_id: "No matches found..."}]})
        }
      } else {
        this.setState({students: [{st_id: null}]})
      }
    }
  }

  select (student) {
    if ('name' in student) {
      this.props.unsaved('voter', student)
      this.props.miniview('register')
    }
  }

  render () {
    return (
      <div className="Search miniview">
        <h3>Find Student By Student Id</h3>
        <br/>
        <FormControl placeholder="Student ID" type="text" onChange = {(event) => this.search(event.target.value)} />
        <div className="results">
          {
            this.state.students.map((student) => {
              return (
                <div key = { student.st_id } className="result" onClick = {(event) => this.select(student) } >{ student.st_id }</div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    data: state.data,
    settings: state.settings
  }
}

export default connect(mapStatetoProps, { unsaved, miniview })(Search)
