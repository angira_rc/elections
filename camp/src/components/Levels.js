import React, { Component } from 'react'
import { FormControl } from 'react-bootstrap'
import { connect } from 'react-redux'
import { show_hide } from '../modules'
import Menu from './Menu'

class Levels extends Component {
  validate () {

  }

  render () {
    return (
      <div className="Levels view">
        <Menu />
        <div className="content">
          <h2>Levels</h2>
          <br/>
          <div className="divisions">
            <div className="part">Name</div>
            <div className="part cats">Categories</div>
            <div className="part">Status</div>
            <div className="part action"></div>
            <div className="part action"></div>
            <div className="part action"></div>
          </div>
          <br/>
          {
            this.props.data.levels.map(level => {
              return (
                <div key={ level.name } className="level">
                  <div key={ level.id } className="divisions parent">
                    <div className="part">
                      <FormControl defaultValue = { level.name } onBlur = {(event) => this.validate(event.target.value, level.id) } />
                    </div>
                    <div className="part cats">{ Object.keys(level.categories).length }</div>
                    <div className="part">{ level.status }</div>
                    <div className="part action">Save</div>
                    <div className="part action delete">&#10008;</div>
                    <div className="part action expand" onClick={(event) => show_hide(level.name, event.target) }>≚</div>
                  </div>
                  <div className="categories subview" id={ level.name }>
                    <h2>Categories</h2>
                    <div className="sub categs">
                      <div className="divisions">
                        <div className="part">Name</div>
                        <div className="part cats">Candidates</div>
                        <div className="part action"></div>
                        <div className="part action"></div>
                        <div className="part action"></div>
                      </div>
                      <br/>
                      {
                        Object.keys(level.categories).map(key => {
                          return (
                            <div key={ key } className="category">
                              <div className="cat-details divisions parent">
                                <div className="part">
                                  <FormControl defaultValue={ key } placeholder="Name" onBlur = {(event) => this.validate({type: "candidate", item: "name", value: event.target.value}) } />
                                </div>
                                <div className="part cats">{ level.categories[key].length }</div>
                                <div className="part action">Save</div>
                                <div className="part action delete">&#10008;</div>
                                <div className="part action expand" onClick={(event) => show_hide(key, event.target)} >≚</div>
                              </div>
                              <div className="candidates subview" id = { key }>
                                <h2>Candidates</h2>
                                <div className="divisions">
                                  <div className="part">Name</div>
                                  <div className="part cats">Image</div>
                                  <div className="part action"></div>
                                  <div className="part action"></div>
                                </div>
                                <br/>
                                {
                                    Object.keys(level.categories[key]).map(candidate => {
                                    return (
                                      <div key = { candidate.nickname } className="candidate">
                                        <div className="cand-details divisions">
                                          <div className="part"><FormControl defaultValue={ candidate.name } /></div>
                                          <div className="part"><FormControl defaultValue={ candidate.nickname } /></div>
                                          <div className="part cats">{ candidate.image }</div>
                                          <div className="part action">Save</div>
                                          <div className="part action delete">&#10008;</div>
                                        </div>
                                      </div>
                                    )
                                  })
                                }
                                <h3>Add New</h3>
                                <div className="divisions parent">
                                  <div className="part">
                                    <FormControl placeholder="Name" onBlur = {(event) => this.validate({type: "candidate", item: "name", value: event.target.value}) } />
                                  </div>
                                  <div className="part">
                                    <FormControl placeholder="Image" onBlur = {(event) => this.validate({type: "candidate", item: "image", value: event.target.value}) } />
                                  </div>
                                  <div className="part action">Save</div>
                                  <div className="part action">Clear</div>
                                </div>
                              </div>
                            </div>
                          )
                        })
                      }
                      <h3>Add New</h3>
                      <div className="divisions parent">
                        <div className="part">
                          <FormControl placeholder="Name" onBlur = {(event) => this.validate({type: "level", item: "name", value: event.target.value}) } />
                        </div>
                        <div className="part action">Save</div>
                        <div className="part action">Clear</div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }
          <div className="level">
            <div className="divisions parent">
              <div className="part label">New</div>
              <div className="part">
                <FormControl placeholder="Name" onBlur = {(event) => this.validate({type: "level", item: "name", value: event.target.value}) } />
              </div>
              <div className="part action">Save</div>
              <div className="part action">Clear</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    data: state.data,
    settings: state.settings
  }
}

export default connect(mapStatetoProps, null)(Levels)
