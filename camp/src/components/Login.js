import React, { Component } from 'react'
import { connect } from 'react-redux'
import { request } from '../modules'
import { Form, FormControl, ControlLabel, Button } from 'react-bootstrap'
import Validate from './Validate'
import { login, new_view, getLevels, getUsers, getVoters } from '../actions'
import './Login.css'

class Login extends Component {
  constructor () {
    super()
    this.state = {
      uname: null,
      pwd: null,
      ago: { status: false, msg: "" }
    }
  }

  async fetch_data (type) {
    let req = await request(`${this.props.settings.url}/levels`, 'GET')
    this.props.getLevels(req.response.data)
    if (type === 'admin') {
      req = await request(`${this.props.settings.url}/users`, 'GET')
      this.props.getUsers(req.response.data)
    }
  }

  validate () {
    return true
  }

  async authenticate () {
    if (this.validate()) {
      this.setState({ago: { status: true, msg: "" }})
      let data = { uname: this.state.uname, pwd: this.state.pwd }
      let req = await request(`${this.props.settings.url}/login`, 'POST', data)
      let response
      // if (typeof(req) == 'undefined') {
        response = req.response
      // } else {
      //   response = { status: false, msg: "There was an error communicating with the server." }
      // }

      if (response.status) {
        this.setState({ago: { status: response.status, msg: '' }})
        document.getElementsByClassName('Login')[0].style.background = 'linear-gradient(-45deg, #002661, #92ffc0)'
        this.props.login({ type: response.data.type, uname: response.data.uname, name: response.data.name, station: response.data.station })
        await this.fetch_data(response.data.type)
        // this.props.getVoters(this.props.settings.url)
        setTimeout(() => this.props.new_view({ usr: response.data.type, view: response.data.type }), 1000)
      } else {
        this.setState({ago: { status: response.status, msg: response.msg }})
        document.getElementsByClassName('Login')[0].style.background = 'linear-gradient(-45deg, #360940, #f05f57)'
      }
    } else {
      this.setState({ago: { status: false, msg: "Check Your Values" }})
    }
  }

  render () {
    return (
      <div className="Login">
        <br/>
        <div className="brand">
          <h2>Elections</h2>
        </div>
        <Form inline  className="login-form">
          <Validate ago = { this.state.ago }/>
          <ControlLabel>UserName</ControlLabel>
          <FormControl
            type="text"
            onBlur = { event => this.setState({ uname: event.target.value})}
            />
          <br/>
          <ControlLabel>Password</ControlLabel>
          <FormControl
            type="password"
            onBlur={ event => this.setState({ pwd: event.target.value }) }
            />
          <br/>
          <Button
            className="form-control button"
            onClick={() => this.authenticate() }>
            Log In
          </Button>
        </Form>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    uname: state.accounts,
    settings: state.settings
  }
}

export default connect(mapStatetoProps, { login, new_view, getLevels, getUsers, getVoters })(Login)
