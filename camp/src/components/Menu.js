import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subview, getLevels, getUsers, getVoters } from '../actions'

class Menu extends Component {
  send () {
    this.props.getLevels('http://localhost:5000')
  }

  render () {
    return (
      <div className="Menu">
        <div className="account">
          <h2>{ this.props.account.uname }</h2>
          <h3>{ this.props.account.name }</h3>
          <a id="logout">Logout</a>
        </div>
        <div className="tabs">
          {
            this.props.views.menu.map((item) => {
              return (
                <a key={ item.id } dir={ item.id } onClick={(event) => this.props.subview(this.props.views.current, item.id) }>{ item.name }</a>
              )
            })
          }
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    views: state.views,
    account: state.accounts
  }
}

export default connect(mapStatetoProps, { subview, getLevels, getUsers, getVoters })(Menu)
