import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subview } from '../actions'
import AccBar from './AccBar'
import './StationMenu.css'

class StationMenu extends Component {
  render () {
    return (
      <div className="StationMenu view">
        <AccBar />
        <h2>Station Menu</h2>
        <div className="menu-items">
          {
            this.props.views.menu.map((view) => {
              return (
                <div key = { view.id } className = { `station-item ${view.id}` } onClick = {(event) => this.props.subview('station', view.id) }>
                  <br/>
                  <img src={ `./res/${view.id}.png` } />
                  <p>{ view.name }</p>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

function mapStatetoProps (state) {
  return {
    views: state.views
  }
}

export default connect(mapStatetoProps, { subview })(StationMenu)
