import React, { Component } from 'react'

class Validate extends Component {
  render () {
    let msg = this.props.ago.msg
    return(
      <div className="Validate">
        { msg }
      </div>
    )
  }
}

export default Validate
