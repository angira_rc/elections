'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRedux = require('react-redux');

var _modules = require('../modules');

var _Menu = require('./Menu');

var _Menu2 = _interopRequireDefault(_Menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Levels = function (_Component) {
  _inherits(Levels, _Component);

  function Levels() {
    _classCallCheck(this, Levels);

    return _possibleConstructorReturn(this, (Levels.__proto__ || Object.getPrototypeOf(Levels)).apply(this, arguments));
  }

  _createClass(Levels, [{
    key: 'validate',
    value: function validate() {}
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'Levels view' },
        _react2.default.createElement(_Menu2.default, null),
        _react2.default.createElement(
          'div',
          { className: 'content' },
          _react2.default.createElement(
            'h2',
            null,
            'Levels'
          ),
          _react2.default.createElement('br', null),
          _react2.default.createElement(
            'div',
            { className: 'divisions' },
            _react2.default.createElement(
              'div',
              { className: 'part' },
              'Name'
            ),
            _react2.default.createElement(
              'div',
              { className: 'part cats' },
              'Categories'
            ),
            _react2.default.createElement(
              'div',
              { className: 'part' },
              'Status'
            ),
            _react2.default.createElement('div', { className: 'part action' }),
            _react2.default.createElement('div', { className: 'part action' }),
            _react2.default.createElement('div', { className: 'part action' })
          ),
          _react2.default.createElement('br', null),
          this.props.data.levels.map(function (level) {
            return _react2.default.createElement(
              'div',
              { key: level.name, className: 'level' },
              _react2.default.createElement(
                'div',
                { key: level.id, className: 'divisions parent' },
                _react2.default.createElement(
                  'div',
                  { className: 'part' },
                  _react2.default.createElement(_reactBootstrap.FormControl, { defaultValue: level.name, onBlur: function onBlur(event) {
                      return _this2.validate(event.target.value, level.id);
                    } })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part cats' },
                  Object.keys(level.categories).length
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part' },
                  level.status
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action' },
                  'Save'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action delete' },
                  '\u2718'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action expand', onClick: function onClick(event) {
                      return (0, _modules.show_hide)(level.name, event.target);
                    } },
                  '\u225A'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'categories subview', id: level.name },
                _react2.default.createElement(
                  'h2',
                  null,
                  'Categories'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'sub categs' },
                  _react2.default.createElement(
                    'div',
                    { className: 'divisions' },
                    _react2.default.createElement(
                      'div',
                      { className: 'part' },
                      'Name'
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'part cats' },
                      'Candidates'
                    ),
                    _react2.default.createElement('div', { className: 'part action' }),
                    _react2.default.createElement('div', { className: 'part action' }),
                    _react2.default.createElement('div', { className: 'part action' })
                  ),
                  _react2.default.createElement('br', null),
                  Object.keys(level.categories).map(function (key) {
                    return _react2.default.createElement(
                      'div',
                      { key: key, className: 'category' },
                      _react2.default.createElement(
                        'div',
                        { className: 'cat-details divisions parent' },
                        _react2.default.createElement(
                          'div',
                          { className: 'part' },
                          _react2.default.createElement(_reactBootstrap.FormControl, { defaultValue: key, placeholder: 'Name', onBlur: function onBlur(event) {
                              return _this2.validate({ type: "candidate", item: "name", value: event.target.value });
                            } })
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'part cats' },
                          level.categories[key].length
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'part action' },
                          'Save'
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'part action delete' },
                          '\u2718'
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'part action expand', onClick: function onClick(event) {
                              return (0, _modules.show_hide)(key, event.target);
                            } },
                          '\u225A'
                        )
                      ),
                      _react2.default.createElement(
                        'div',
                        { className: 'candidates subview', id: key },
                        _react2.default.createElement(
                          'h2',
                          null,
                          'Candidates'
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'divisions' },
                          _react2.default.createElement(
                            'div',
                            { className: 'part' },
                            'Name'
                          ),
                          _react2.default.createElement(
                            'div',
                            { className: 'part cats' },
                            'Image'
                          ),
                          _react2.default.createElement('div', { className: 'part action' }),
                          _react2.default.createElement('div', { className: 'part action' })
                        ),
                        _react2.default.createElement('br', null),
                        Object.keys(level.categories[key]).map(function (candidate) {
                          return _react2.default.createElement(
                            'div',
                            { key: candidate.nickname, className: 'candidate' },
                            _react2.default.createElement(
                              'div',
                              { className: 'cand-details divisions' },
                              _react2.default.createElement(
                                'div',
                                { className: 'part' },
                                _react2.default.createElement(_reactBootstrap.FormControl, { defaultValue: candidate.name })
                              ),
                              _react2.default.createElement(
                                'div',
                                { className: 'part' },
                                _react2.default.createElement(_reactBootstrap.FormControl, { defaultValue: candidate.nickname })
                              ),
                              _react2.default.createElement(
                                'div',
                                { className: 'part cats' },
                                candidate.image
                              ),
                              _react2.default.createElement(
                                'div',
                                { className: 'part action' },
                                'Save'
                              ),
                              _react2.default.createElement(
                                'div',
                                { className: 'part action delete' },
                                '\u2718'
                              )
                            )
                          );
                        }),
                        _react2.default.createElement(
                          'h3',
                          null,
                          'Add New'
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'divisions parent' },
                          _react2.default.createElement(
                            'div',
                            { className: 'part' },
                            _react2.default.createElement(_reactBootstrap.FormControl, { placeholder: 'Name', onBlur: function onBlur(event) {
                                return _this2.validate({ type: "candidate", item: "name", value: event.target.value });
                              } })
                          ),
                          _react2.default.createElement(
                            'div',
                            { className: 'part' },
                            _react2.default.createElement(_reactBootstrap.FormControl, { placeholder: 'Image', onBlur: function onBlur(event) {
                                return _this2.validate({ type: "candidate", item: "image", value: event.target.value });
                              } })
                          ),
                          _react2.default.createElement(
                            'div',
                            { className: 'part action' },
                            'Save'
                          ),
                          _react2.default.createElement(
                            'div',
                            { className: 'part action' },
                            'Clear'
                          )
                        )
                      )
                    );
                  }),
                  _react2.default.createElement(
                    'h3',
                    null,
                    'Add New'
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'divisions parent' },
                    _react2.default.createElement(
                      'div',
                      { className: 'part' },
                      _react2.default.createElement(_reactBootstrap.FormControl, { placeholder: 'Name', onBlur: function onBlur(event) {
                          return _this2.validate({ type: "level", item: "name", value: event.target.value });
                        } })
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'part action' },
                      'Save'
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'part action' },
                      'Clear'
                    )
                  )
                )
              )
            );
          }),
          _react2.default.createElement(
            'div',
            { className: 'level' },
            _react2.default.createElement(
              'div',
              { className: 'divisions parent' },
              _react2.default.createElement(
                'div',
                { className: 'part label' },
                'New'
              ),
              _react2.default.createElement(
                'div',
                { className: 'part' },
                _react2.default.createElement(_reactBootstrap.FormControl, { placeholder: 'Name', onBlur: function onBlur(event) {
                    return _this2.validate({ type: "level", item: "name", value: event.target.value });
                  } })
              ),
              _react2.default.createElement(
                'div',
                { className: 'part action' },
                'Save'
              ),
              _react2.default.createElement(
                'div',
                { className: 'part action' },
                'Clear'
              )
            )
          )
        )
      );
    }
  }]);

  return Levels;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    data: state.data,
    settings: state.settings
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, null)(Levels);