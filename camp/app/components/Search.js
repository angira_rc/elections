'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRedux = require('react-redux');

var _actions = require('../actions');

var _modules = require('../modules');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Search = function (_Component) {
  _inherits(Search, _Component);

  function Search() {
    _classCallCheck(this, Search);

    var _this = _possibleConstructorReturn(this, (Search.__proto__ || Object.getPrototypeOf(Search)).call(this));

    _this.state = {
      students: []
    };
    return _this;
  }

  _createClass(Search, [{
    key: 'search',
    value: async function search(val) {
      if (val) {
        if (val.length > 5) {
          var req = await (0, _modules.request)(this.props.settings.url + '/students', 'POST', { id: val });
          var response = req.response;
          if (response.data) {
            this.setState({ students: response.data });
          } else {
            this.setState({ students: [{ st_id: "No matches found..." }] });
          }
        } else {
          this.setState({ students: [{ st_id: null }] });
        }
      }
    }
  }, {
    key: 'select',
    value: function select(student) {
      if ('name' in student) {
        this.props.unsaved('voter', student);
        this.props.miniview('register');
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'Search miniview' },
        _react2.default.createElement(
          'h3',
          null,
          'Find Student By Student Id'
        ),
        _react2.default.createElement('br', null),
        _react2.default.createElement(_reactBootstrap.FormControl, { placeholder: 'Student ID', type: 'text', onChange: function onChange(event) {
            return _this2.search(event.target.value);
          } }),
        _react2.default.createElement(
          'div',
          { className: 'results' },
          this.state.students.map(function (student) {
            return _react2.default.createElement(
              'div',
              { key: student.st_id, className: 'result', onClick: function onClick(event) {
                  return _this2.select(student);
                } },
              student.st_id
            );
          })
        )
      );
    }
  }]);

  return Search;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    data: state.data,
    settings: state.settings
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, { unsaved: _actions.unsaved, miniview: _actions.miniview })(Search);