'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _modules = require('../modules');

var _AccBar = require('./AccBar');

var _AccBar2 = _interopRequireDefault(_AccBar);

var _Search = require('./Search');

var _Search2 = _interopRequireDefault(_Search);

var _RegForm = require('./RegForm');

var _RegForm2 = _interopRequireDefault(_RegForm);

var _Menu = require('./Menu');

var _Menu2 = _interopRequireDefault(_Menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Registration = function (_Component) {
  _inherits(Registration, _Component);

  function Registration() {
    _classCallCheck(this, Registration);

    return _possibleConstructorReturn(this, (Registration.__proto__ || Object.getPrototypeOf(Registration)).apply(this, arguments));
  }

  _createClass(Registration, [{
    key: 'registered',
    value: async function registered() {
      // this.setState({ago: { status: true, msg: "" }})
      // let data = { uname: this.state.uname, pwd: this.state.pwd }
      var req = await (0, _modules.request)(this.props.settings.url + '/voters/' + this.props.accounts.station, 'GET');
      var response = req.response;
      console.log(response);
    }
  }, {
    key: 'componentWillMount',
    value: async function componentWillMount() {
      await this.registered();
    }
  }, {
    key: 'render',
    value: function render() {
      switch (this.props.views.miniview) {
        case "register":
          return _react2.default.createElement(
            'div',
            { className: 'Registration view' },
            _react2.default.createElement(_Menu2.default, null),
            _react2.default.createElement(
              'div',
              { className: 'content' },
              _react2.default.createElement(
                'h2',
                null,
                'Registration'
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(_RegForm2.default, null)
            )
          );

        case "registered":
          return _react2.default.createElement(
            'div',
            { className: 'Registration view' },
            _react2.default.createElement(_Menu2.default, null),
            _react2.default.createElement(
              'div',
              { className: 'content' },
              _react2.default.createElement(
                'h2',
                null,
                'Registered Voters'
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(_RegForm2.default, null)
            )
          );

        default:
          return _react2.default.createElement(
            'div',
            { className: 'Registration view' },
            _react2.default.createElement(_Menu2.default, null),
            _react2.default.createElement(
              'div',
              { className: 'content' },
              _react2.default.createElement(
                'h2',
                null,
                'Registration'
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(_Search2.default, null)
            )
          );
      }
    }
  }]);

  return Registration;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    data: state.data,
    accounts: state.accounts,
    settings: state.settings,
    views: state.views
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, null)(Registration);