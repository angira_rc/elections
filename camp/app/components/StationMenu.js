'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _actions = require('../actions');

var _AccBar = require('./AccBar');

var _AccBar2 = _interopRequireDefault(_AccBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StationMenu = function (_Component) {
  _inherits(StationMenu, _Component);

  function StationMenu() {
    _classCallCheck(this, StationMenu);

    return _possibleConstructorReturn(this, (StationMenu.__proto__ || Object.getPrototypeOf(StationMenu)).apply(this, arguments));
  }

  _createClass(StationMenu, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'StationMenu view' },
        _react2.default.createElement(_AccBar2.default, null),
        _react2.default.createElement(
          'h2',
          null,
          'Station Menu'
        ),
        _react2.default.createElement(
          'div',
          { className: 'menu-items' },
          this.props.views.menu.map(function (view) {
            return _react2.default.createElement(
              'div',
              { key: view.id, className: 'station-item ' + view.id, onClick: function onClick(event) {
                  return _this2.props.subview('station', view.id);
                } },
              _react2.default.createElement('br', null),
              _react2.default.createElement('img', { src: './res/' + view.id + '.png' }),
              _react2.default.createElement(
                'p',
                null,
                view.name
              )
            );
          })
        )
      );
    }
  }]);

  return StationMenu;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    views: state.views
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, { subview: _actions.subview })(StationMenu);