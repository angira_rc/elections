'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRedux = require('react-redux');

var _modules = require('../modules');

var _Menu = require('./Menu');

var _Menu2 = _interopRequireDefault(_Menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Users = function (_Component) {
  _inherits(Users, _Component);

  function Users() {
    _classCallCheck(this, Users);

    return _possibleConstructorReturn(this, (Users.__proto__ || Object.getPrototypeOf(Users)).apply(this, arguments));
  }

  _createClass(Users, [{
    key: 'validate',
    value: function validate() {}
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'Users view' },
        _react2.default.createElement(_Menu2.default, null),
        _react2.default.createElement(
          'div',
          { className: 'content' },
          _react2.default.createElement(
            'h2',
            null,
            'Users'
          ),
          _react2.default.createElement('br', null),
          _react2.default.createElement(
            'div',
            { className: 'divisions' },
            _react2.default.createElement(
              'div',
              { className: 'part' },
              'Name'
            ),
            _react2.default.createElement(
              'div',
              { className: 'part action' },
              'UserName'
            ),
            _react2.default.createElement('div', { className: 'part action' }),
            _react2.default.createElement('div', { className: 'part action' }),
            _react2.default.createElement('div', { className: 'part action' })
          ),
          _react2.default.createElement('br', null),
          this.props.data.users.map(function (user) {
            return _react2.default.createElement(
              'div',
              { key: user.uname, className: 'user' },
              _react2.default.createElement(
                'div',
                { className: 'divisions parent' },
                _react2.default.createElement(
                  'div',
                  { className: 'part' },
                  user.name
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part' },
                  user.uname
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action' },
                  'Save'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action delete' },
                  '\u2718'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'part action expand', onClick: function onClick(event) {
                      return (0, _modules.show_hide)(user.uname, event.target);
                    } },
                  '\u225A'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'user-details subview', id: user.uname },
                _react2.default.createElement(
                  'div',
                  { className: 'details' },
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Full Name'
                  ),
                  _react2.default.createElement(_reactBootstrap.FormControl, {
                    defaultValue: user.name,
                    placeholder: 'Full Name',
                    onBlur: function onBlur(event) {
                      return _this2.validate(event.target.value, user.name);
                    } }),
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'UserName'
                  ),
                  _react2.default.createElement(_reactBootstrap.FormControl, {
                    defaultValue: user.uname,
                    placeholder: 'UserName',
                    onBlur: function onBlur(event) {
                      return _this2.validate(event.target.value, user.uname);
                    } }),
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Station'
                  ),
                  _react2.default.createElement(_reactBootstrap.FormControl, {
                    defaultValue: user.station,
                    placeholder: 'Station',
                    onBlur: function onBlur(event) {
                      return _this2.validate(event.target.value, user.station);
                    } }),
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Status'
                  ),
                  _react2.default.createElement(
                    _reactBootstrap.FormControl,
                    {
                      componentClass: 'select',
                      defaultValue: user.type,
                      onBlur: function onBlur(event) {
                        return _this2.validate(event.target.value, user.name);
                      } },
                    _react2.default.createElement(
                      'option',
                      { value: 'active' },
                      'active'
                    ),
                    _react2.default.createElement(
                      'option',
                      { value: 'inactive' },
                      'inactive'
                    )
                  )
                )
              )
            );
          }),
          _react2.default.createElement(
            'div',
            { className: 'user' },
            _react2.default.createElement(
              'div',
              { className: 'divisions parent' },
              _react2.default.createElement(
                'div',
                { className: 'part' },
                'Add New'
              ),
              _react2.default.createElement(
                'div',
                { className: 'part action' },
                'Save'
              ),
              _react2.default.createElement(
                'div',
                { className: 'part action' },
                'Clear'
              ),
              _react2.default.createElement(
                'div',
                { className: 'part action expand', onClick: function onClick(event) {
                    return (0, _modules.show_hide)("new", event.target);
                  } },
                '\u271A'
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'user-details subview', id: 'new' },
              _react2.default.createElement(
                'div',
                { className: 'details' },
                _react2.default.createElement(_reactBootstrap.FormControl, {
                  placeholder: 'Full Name',
                  onBlur: function onBlur(event) {
                    return _this2.validate(event.target.value, "new");
                  } }),
                _react2.default.createElement(_reactBootstrap.FormControl, {
                  placeholder: 'UserName',
                  onBlur: function onBlur(event) {
                    return _this2.validate(event.target.value, "new");
                  } }),
                _react2.default.createElement(_reactBootstrap.FormControl, {
                  type: 'password',
                  placeholder: 'Password',
                  onBlur: function onBlur(event) {
                    return _this2.validate(event.target.value, "new");
                  } }),
                _react2.default.createElement(_reactBootstrap.FormControl, {
                  placeholder: 'Station',
                  onBlur: function onBlur(event) {
                    return _this2.validate(event.target.value, "new");
                  } })
              )
            )
          )
        )
      );
    }
  }]);

  return Users;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    data: state.data,
    settings: state.settings
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, null)(Users);