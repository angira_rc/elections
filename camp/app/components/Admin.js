'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _Levels = require('./Levels');

var _Levels2 = _interopRequireDefault(_Levels);

var _Users = require('./Users');

var _Users2 = _interopRequireDefault(_Users);

var _AdmElections = require('./AdmElections');

var _AdmElections2 = _interopRequireDefault(_AdmElections);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Admin = function (_Component) {
  _inherits(Admin, _Component);

  function Admin() {
    _classCallCheck(this, Admin);

    return _possibleConstructorReturn(this, (Admin.__proto__ || Object.getPrototypeOf(Admin)).apply(this, arguments));
  }

  _createClass(Admin, [{
    key: 'render',
    value: function render() {
      switch (this.props.view.current) {
        case 'levels':
          return _react2.default.createElement(_Levels2.default, null);

        case 'users':
          return _react2.default.createElement(_Users2.default, null);

        case 'elections':
          return _react2.default.createElement(_AdmElections2.default, null);

        default:
          return _react2.default.createElement(_Levels2.default, null);
      }
    }
  }]);

  return Admin;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    view: state.views
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, null)(Admin);