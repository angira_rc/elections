'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRedux = require('react-redux');

var _actions = require('../actions');

var _modules = require('../modules');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RegForm = function (_Component) {
  _inherits(RegForm, _Component);

  function RegForm() {
    _classCallCheck(this, RegForm);

    return _possibleConstructorReturn(this, (RegForm.__proto__ || Object.getPrototypeOf(RegForm)).apply(this, arguments));
  }

  _createClass(RegForm, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'RegForm miniview' },
        _react2.default.createElement(
          _reactBootstrap.Form,
          { id: 'RegForm' },
          _react2.default.createElement(
            'table',
            null,
            _react2.default.createElement(
              'tbody',
              null,
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Student Id'
                  )
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(_reactBootstrap.FormControl, { disabled: true, value: this.props.data.unsaved.voter.st_id })
                )
              ),
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Name'
                  )
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(_reactBootstrap.FormControl, { disabled: true, value: this.props.data.unsaved.voter.name })
                )
              ),
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactBootstrap.ControlLabel,
                    null,
                    'Level'
                  )
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactBootstrap.FormControl,
                    {
                      componentClass: 'select' },
                    this.props.data.levels.map(function (level) {
                      return _react2.default.createElement(
                        'option',
                        { key: level.name, value: level.name },
                        level.name
                      );
                    })
                  )
                )
              ),
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactBootstrap.Button,
                    null,
                    'Save'
                  )
                ),
                _react2.default.createElement('td', null)
              )
            )
          )
        )
      );
    }
  }]);

  return RegForm;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    data: state.data
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, { save: _actions.save })(RegForm);