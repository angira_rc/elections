'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _Levels = require('./Levels');

var _Levels2 = _interopRequireDefault(_Levels);

var _Users = require('./Users');

var _Users2 = _interopRequireDefault(_Users);

var _StationMenu = require('./StationMenu');

var _StationMenu2 = _interopRequireDefault(_StationMenu);

var _Registration = require('./Registration');

var _Registration2 = _interopRequireDefault(_Registration);

var _StatElections = require('./StatElections');

var _StatElections2 = _interopRequireDefault(_StatElections);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Station = function (_Component) {
  _inherits(Station, _Component);

  function Station() {
    _classCallCheck(this, Station);

    return _possibleConstructorReturn(this, (Station.__proto__ || Object.getPrototypeOf(Station)).apply(this, arguments));
  }

  _createClass(Station, [{
    key: 'render',
    value: function render() {
      switch (this.props.view.current) {
        case 'voter-reg':
          return _react2.default.createElement(_Registration2.default, null);

        case 'elections':
          return _react2.default.createElement(_StatElections2.default, null);

        default:
          return _react2.default.createElement(_Registration2.default, null);
      }
    }
  }]);

  return Station;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    view: state.views
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, null)(Station);