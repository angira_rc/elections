'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _modules = require('../modules');

var _reactBootstrap = require('react-bootstrap');

var _Validate = require('./Validate');

var _Validate2 = _interopRequireDefault(_Validate);

var _actions = require('../actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Login = function (_Component) {
  _inherits(Login, _Component);

  function Login() {
    _classCallCheck(this, Login);

    var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this));

    _this.state = {
      uname: null,
      pwd: null,
      ago: { status: false, msg: "" }
    };
    return _this;
  }

  _createClass(Login, [{
    key: 'fetch_data',
    value: async function fetch_data(type) {
      var req = await (0, _modules.request)(this.props.settings.url + '/levels', 'GET');
      this.props.getLevels(req.response.data);
      if (type === 'admin') {
        req = await (0, _modules.request)(this.props.settings.url + '/users', 'GET');
        this.props.getUsers(req.response.data);
      }
    }
  }, {
    key: 'validate',
    value: function validate() {
      return true;
    }
  }, {
    key: 'authenticate',
    value: async function authenticate() {
      var _this2 = this;

      if (this.validate()) {
        this.setState({ ago: { status: true, msg: "" } });
        var data = { uname: this.state.uname, pwd: this.state.pwd };
        var req = await (0, _modules.request)(this.props.settings.url + '/login', 'POST', data);
        var response = void 0;
        // if (typeof(req) == 'undefined') {
        response = req.response;
        // } else {
        //   response = { status: false, msg: "There was an error communicating with the server." }
        // }

        if (response.status) {
          this.setState({ ago: { status: response.status, msg: '' } });
          document.getElementsByClassName('Login')[0].style.background = 'linear-gradient(-45deg, #002661, #92ffc0)';
          this.props.login({ type: response.data.type, uname: response.data.uname, name: response.data.name, station: response.data.station });
          await this.fetch_data(response.data.type);
          // this.props.getVoters(this.props.settings.url)
          setTimeout(function () {
            return _this2.props.new_view({ usr: response.data.type, view: response.data.type });
          }, 1000);
        } else {
          this.setState({ ago: { status: response.status, msg: response.msg } });
          document.getElementsByClassName('Login')[0].style.background = 'linear-gradient(-45deg, #360940, #f05f57)';
        }
      } else {
        this.setState({ ago: { status: false, msg: "Check Your Values" } });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        { className: 'Login' },
        _react2.default.createElement('br', null),
        _react2.default.createElement(
          'div',
          { className: 'brand' },
          _react2.default.createElement(
            'h2',
            null,
            'Elections'
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Form,
          { inline: true, className: 'login-form' },
          _react2.default.createElement(_Validate2.default, { ago: this.state.ago }),
          _react2.default.createElement(
            _reactBootstrap.ControlLabel,
            null,
            'UserName'
          ),
          _react2.default.createElement(_reactBootstrap.FormControl, {
            type: 'text',
            onBlur: function onBlur(event) {
              return _this3.setState({ uname: event.target.value });
            }
          }),
          _react2.default.createElement('br', null),
          _react2.default.createElement(
            _reactBootstrap.ControlLabel,
            null,
            'Password'
          ),
          _react2.default.createElement(_reactBootstrap.FormControl, {
            type: 'password',
            onBlur: function onBlur(event) {
              return _this3.setState({ pwd: event.target.value });
            }
          }),
          _react2.default.createElement('br', null),
          _react2.default.createElement(
            _reactBootstrap.Button,
            {
              className: 'form-control button',
              onClick: function onClick() {
                return _this3.authenticate();
              } },
            'Log In'
          )
        )
      );
    }
  }]);

  return Login;
}(_react.Component);

function mapStatetoProps(state) {
  return {
    uname: state.accounts,
    settings: state.settings
  };
}

exports.default = (0, _reactRedux.connect)(mapStatetoProps, { login: _actions.login, new_view: _actions.new_view, getLevels: _actions.getLevels, getUsers: _actions.getUsers, getVoters: _actions.getVoters })(Login);