'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = login;
exports.logout = logout;
exports.new_view = new_view;
exports.subview = subview;
exports.miniview = miniview;
exports.setUrl = setUrl;
exports.getLevels = getLevels;
exports.getUsers = getUsers;
exports.getVoters = getVoters;
exports.unsaved = unsaved;
exports.save = save;
var LOGIN = exports.LOGIN = 'LOGIN';
var LOGOUT = exports.LOGOUT = 'LOGOUT';
var NEW_VIEW = exports.NEW_VIEW = 'NEW_VIEW';
var SET_URL = exports.SET_URL = 'SET_URL';
var SUB_VIEW = exports.SUB_VIEW = 'SUB_VIEW';
var MINI_VIEW = exports.MINI_VIEW = 'MINI_VIEW';
var GET_LEVELS = exports.GET_LEVELS = 'GET_LEVELS';
var GET_USERS = exports.GET_USERS = 'GET_USERS';
var GET_VOTERS = exports.GET_VOTERS = 'GET_VOTERS';
var SET_NEW = exports.SET_NEW = 'SET_NEW';
var SAVE = exports.SAVE = 'SAVE';

function login(usr) {
  var action = {
    type: LOGIN,
    usr: usr
  };
  return action;
}

function logout(usr) {
  var action = {
    type: LOGOUT,
    usr: usr
  };
  return action;
}

function new_view(arg) {
  var action = {
    type: NEW_VIEW,
    usr: arg.usr,
    view: arg.view
  };
  return action;
}

function subview(parent, sub) {
  var action = {
    type: SUB_VIEW,
    parent: parent,
    subview: sub
  };
  return action;
}

function miniview(m_view) {
  var action = {
    type: MINI_VIEW,
    miniview: m_view
  };
  return action;
}

function setUrl(url) {
  var action = {
    type: SET_URL,
    url: url
  };
  return action;
}

function getLevels(data) {
  var action = {
    type: GET_LEVELS,
    data: data
  };
  return action;
}

function getUsers(data) {
  var action = {
    type: GET_USERS,
    data: data
  };
  return action;
}

function getVoters(data) {
  var action = {
    type: GET_VOTERS,
    data: data
  };
  return action;
}

function unsaved(item, data) {
  var action = {
    type: SET_NEW,
    item: item,
    data: data
  };
  return action;
}

function save(item) {
  var action = {
    type: SAVE,
    item: item
  };
  return action;
}