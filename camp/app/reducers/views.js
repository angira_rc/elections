'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actions = require('../actions');

function views() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { name: 'login', menu: [], current: 'login', miniview: null };
  var action = arguments[1];

  switch (action.type) {
    case _actions.NEW_VIEW:
      switch (action.usr) {
        case 'admin':
          return _extends({}, state, {
            name: 'admin',
            menu: [{ id: 'levels', name: 'Levels' }, { id: 'users', name: 'Users' }, { id: 'elections', name: 'Elections' }],
            current: 'levels'
          });

        case 'active':
          return _extends({}, state, {
            name: 'station',
            menu: [{ id: 'voter-reg', name: 'Voter Registration' }, { id: 'elections', name: 'Elections' }],
            current: ''
          });

        default:
          if (action.view === 'settings') {
            return _extends({}, state, {
              name: 'settings',
              menu: [{ id: 'network', name: 'Network' }],
              current: 'network'
            });
          }

          return state;
      }

    case _actions.SUB_VIEW:
      return _extends({}, state, {
        current: action.subview
      });

    case _actions.MINI_VIEW:
      return _extends({}, state, {
        miniview: action.miniview
      });

    default:
      return state;
  }
}

exports.default = views;