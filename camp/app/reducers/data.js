'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actions = require('../actions');

function data() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { unsaved: { status: false, data: {} } };
  var action = arguments[1];

  switch (action.type) {
    case _actions.GET_LEVELS:
      return _extends({}, state, {
        levels: action.data
      });

    case _actions.GET_USERS:
      return _extends({}, state, {
        users: action.data
      });

    case _actions.GET_VOTERS:
      return _extends({}, state, {
        voters: action.data
      });

    case _actions.SET_NEW:
      var _data = _extends({}, state.unsaved.data);
      _data[action.item] = action.data;

      return _extends({}, state, {
        unsaved: _extends({ status: true }, _data)
      });

    default:
      return state;
  }
}

exports.default = data;