'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actions = require('../actions');

function accounts() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { uname: null, name: null, type: null, station: null };
  var action = arguments[1];

  switch (action.type) {
    case _actions.LOGIN:
      return _extends({}, state, {
        uname: action.usr.uname,
        name: action.usr.name,
        type: action.usr.type,
        station: action.usr.station
      });

    default:
      return state;
  }
}

exports.default = accounts;