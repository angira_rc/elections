'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _accounts = require('./accounts.js');

var _accounts2 = _interopRequireDefault(_accounts);

var _views = require('./views.js');

var _views2 = _interopRequireDefault(_views);

var _settings = require('./settings.js');

var _settings2 = _interopRequireDefault(_settings);

var _data = require('./data.js');

var _data2 = _interopRequireDefault(_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var main = (0, _redux.combineReducers)({
  accounts: _accounts2.default,
  views: _views2.default,
  settings: _settings2.default,
  data: _data2.default
});

exports.default = main;