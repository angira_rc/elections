'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actions = require('../actions');

function settings() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { url: 'http://localhost:5000' };
  var action = arguments[1];

  switch (action.type) {
    case _actions.SET_URL:
      var new_state = _extends({}, state, { url: action.url });
      return new_state;

    default:
      return state;
  }
}

exports.default = settings;