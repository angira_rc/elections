"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.request = request;
exports.show_hide = show_hide;
async function request(url, method) {
  var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  var resp = void 0;
  var request = {
    method: method,
    mode: "cors",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    }
  };

  if (method === 'POST') {
    request = _extends({}, request, { body: JSON.stringify(data) });
  }

  await fetch(url, request).then(function (response) {
    return response.json();
  }).then(function (json) {
    resp = json;
  }).catch(function (error) {
    return resp = { status: false, msg: "There was an error communicating with the server." };
  });

  return resp;
}

function exists(arr, str) {
  for (var pos in arr) {
    if (arr[pos] === str) {
      return true;
    }
  }
  return false;
}

function show_hide(id, starter) {
  var element = document.getElementById(id);
  var classes = element.className.split(" ");

  if (exists(classes, "expanded")) {
    element.classList.remove("expanded");
    setTimeout(function () {
      return element.style.display = 'none';
    }, 400);
  } else {
    element.classList.add("expanded");
    element.style.display = 'block';
  }

  if (starter.innerHTML === '≚') {
    starter.innerHTML = '≙';
  } else if (starter.innerHTML === '≙') {
    starter.innerHTML = '≚';
  }
}