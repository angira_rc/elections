'use strict';

var _require = require('electron'),
    app = _require.app,
    BrowserWindow = _require.BrowserWindow;

var url = require('url');
var path = require('path');

var mainWindow = void 0;

function createWindow() {

  mainWindow = new BrowserWindow({
    width: 930,
    height: 600
  });

  mainWindow.loadFile('./app/index.html');

  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});