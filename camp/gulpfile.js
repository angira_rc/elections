const spawn = require('child_process').spawn
const gulp = require('gulp')
const babel = require('gulp-babel')
const css = require('gulp-css')
const concat = require('gulp-concat')

gulp.task('html', () => {
	return gulp.src('src/index.html')
		.pipe(gulp.dest('app/'));
});

gulp.task('css', function() {
  return gulp.src('src/**/*.css')
    .pipe(css())
    .pipe(concat('main.css'))
    .pipe(gulp.dest('app/'));
});

gulp.task('js', () => {
	return gulp.src(['main.js', 'src/**/*.js'])
		.pipe(babel())
		.pipe(gulp.dest('app/'));
});

gulp.task('images', function() {
	return gulp.src('src/images/*.png')
	.pipe(gulp.dest('app/res/'));
});

gulp.task('start', ['html', 'css', 'js', 'images'], () => {
	spawn(
		'node_modules/.bin/electron',
		['.'],
		{ stdio: 'inherit' }
	).on('close', () => process.exit());
});
